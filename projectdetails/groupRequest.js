(function() {

  $(document).ready(initialize);
  function initialize() {
   $('.sidenav').sidenav();
   $('.chips').chips();
    $('.chips-autocomplete').chips({
      autocompleteOptions: {
        data: {
          'Soprano': null,
          'Alto': null,
          'Tenor': null,
          'Bass' : null,
          'Accordion': null,
          'Acoustic-Guitar': null,
          'Bagpipes': null,
          'Banjo': null,
          'Bass-Guitar': null,
          'Cello': null,
          'Clarinet': null,
          'Double-Bass': null,
          'Drums': null,
          'Electric-Guitar': null,
          'Flute': null,
          'French-Horn': null,
          'Harmonica': null,
          'Harp': null,
          'Keyboard': null,
          'Mandolin': null,
          'Oboe': null,
          'Piano': null,
          'Recorder': null,
          'Saxophone': null,
          'Trombone': null,
          'Trumpet': null,
          'Tuba': null,
          'Ukelele': null,
          'Viola': null,
          'Violin': null,
          'Xylophone': null,
        },
        limit: Infinity,
        minLength: 1
      }
    });
  }


})();
