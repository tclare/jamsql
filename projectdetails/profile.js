var number_skills = 1; // Declare a user's skills as a global variable
(function() {

  $(document).ready(initialize);
  function initialize() {
   $('#subtract-button').addClass("disabled");
   $('.sidenav').sidenav();
   $('select').formSelect();
   $('#add-button').click(addFcn);
   $('#subtract-button').click(removeFcn);
  }

  function addFcn() {
    $('#subtract-button').removeClass("disabled");
    if (number_skills == 9) {
      $('#add-button').addClass("disabled");
    }
    if (number_skills <= 9) {
      number_skills++;
      console.log(number_skills);
      $('#no'+number_skills).css('display','block');
    }
  }

  function removeFcn() {
    $('#add-button').removeClass("disabled");
    if (number_skills == 2) {
      $('#subtract-button').addClass("disabled");
    }
    if (number_skills >=2) {
      $('#no'+number_skills).css('display','none');
      number_skills--;
    }
  }

})();
